package it.mpizzola.activity.github.picasso;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import it.mpizzola.R;


public class CardHolder {

    TextView nameTxt;
    ImageView img;

    public CardHolder(View v) {

        nameTxt= (TextView) v.findViewById(R.id.nameTxt);
        img= (ImageView) v.findViewById(R.id.movieImage);

    }
}
