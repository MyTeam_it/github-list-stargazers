package it.mpizzola.activity.github;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import evo.framework.utility.log.IuLog;
import evo.packages.foundation.IuFoundation;
import evo.packages.github.entity.EGitHubRepository;
import it.mpizzola.R;
import it.mpizzola.activity.github.picasso.CustomAdapter;

public class ActivityListStargazers extends AppCompatActivity {

    ListView listView;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liststargezer);


        try {
            try {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {
                IuLog.Error(e);
            }
            Bundle bundle = getIntent().getExtras();
            String iD = bundle.getString("iD");

            EGitHubRepository eGitHubRepository = (EGitHubRepository) IuFoundation.DoGetOne(iD);

            setTitle(eGitHubRepository.user+"/"+eGitHubRepository.repository);

            listView = (ListView) findViewById(R.id.listview_stargazer);
            adapter = new CustomAdapter(this, eGitHubRepository.arrayListStargazer);

            listView.setAdapter(adapter);
        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home button
                case android.R.id.home:
                    NavUtils.navigateUpFromSameTask(this);
                    return true;
            }
            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            IuLog.Error(e);
        }
        return false;
    }
}