package it.mpizzola.activity.github.picasso;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import evo.packages.github.entity.EGithubUser;
import it.mpizzola.R;


public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<EGithubUser> arrayListEGithubUser;

    LayoutInflater inflater;

    public CustomAdapter(Context c, ArrayList<EGithubUser> arrayListEGithubUser) {
        this.context = c;
        this.arrayListEGithubUser = arrayListEGithubUser;
    }

    @Override
    public int getCount() {
        return arrayListEGithubUser.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListEGithubUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater==null)
        {
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.item_egithubuser,parent,false);

        }

        CardHolder holder=new CardHolder(convertView);
        holder.nameTxt.setText(arrayListEGithubUser.get(position).login);
        Picasso.get().load(arrayListEGithubUser.get(position).avatar_url).into( holder.img);

        return convertView;
    }
}
