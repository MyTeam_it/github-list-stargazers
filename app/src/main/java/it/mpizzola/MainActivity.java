package it.mpizzola;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import evo.framework.IEvoCallback;
import evo.framework.entity.EObject;
import evo.framework.utility.key.IuKey;
import evo.framework.utility.log.IuLog;
import evo.packages.dialog.GDialogAlert;
import evo.packages.dialog.GDialogProgress;
import evo.packages.dialog.GDialogWebview;
import evo.packages.foundation.IuFoundation;
import evo.packages.github.control.CGithubFlow;
import evo.packages.github.entity.EGitHubRepository;
import evo.packages.validator.IuValidator;
import it.mpizzola.activity.github.ActivityListStargazers;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IEvoCallback {

    protected GDialogProgress gDialogProgress;

    protected EditText textInputGitHubUser;
    protected EditText textInputGitHubRepository;
    protected ImageButton buttonStargazer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            IuLog.isProduction = false;

            DoViewToolbar();
            DoViewNavigation();
            DoInitFormStargazer();

        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    protected void DoViewNavigation() {
        try {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    protected void DoInitFormStargazer() {
        try {
            buttonStargazer = (ImageButton) findViewById(R.id.buttonStargazer);
            buttonStargazer.setEnabled(false);
            textInputGitHubUser = (EditText) findViewById(R.id.textInputGitHubUser);
            textInputGitHubRepository = (EditText) findViewById(R.id.textInputGitHubRepository);

            textInputGitHubUser.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    try {

                        DoEnableButtonStargezer();

                    } catch (Exception e) {
                        IuLog.Error(e);
                    }
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {


                }
            });


            textInputGitHubRepository.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    try {

                        DoEnableButtonStargezer();

                    } catch (Exception e) {
                        IuLog.Error(e);
                    }
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                }
            });

        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    protected void DoEnableButtonStargezer(){
        try {

            boolean isEnableButton = (IuValidator.IsValid(textInputGitHubUser.getText()) &&  IuValidator.IsValid(textInputGitHubRepository.getText()));

            buttonStargazer.setEnabled(isEnableButton);
            if (isEnableButton) {
                buttonStargazer.setImageResource(R.drawable.ic_star_button_2);
                buttonStargazer.setBackgroundColor(getResources().getColor(R.color.cardview_light_background,getTheme()));
            }else{
                buttonStargazer.setImageResource(R.drawable.ic_star_button);
                buttonStargazer.setBackgroundColor(getResources().getColor(R.color.cardview_dark_background,getTheme()));
            }

        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    protected void DoViewToolbar() {
        try {

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            try {

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();
            } catch (Exception e) {
                IuLog.Error(e);
            }

        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        } catch (Exception e) {
            IuLog.Error(e);
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {

            int id = item.getItemId();
            if (id == R.id.action_help) {
                GDialogWebview gDialogWebview = new GDialogWebview();
                gDialogWebview.context = this;
                gDialogWebview.title = getResources().getString(R.string.title_help);;
                gDialogWebview.url = getResources().getString(R.string.url_help);;
                gDialogWebview.OnView();

                return true;
            }

            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            IuLog.Error(e);
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        try {
            String title = "";
            String url = "";
            int id = item.getItemId();

            switch (id) {
                case R.id.nav_help: {
                    title = getResources().getString(R.string.title_help);
                    url = getResources().getString(R.string.url_help);
                    break;
                }

                case R.id.nav_github_api: {
                    title = getResources().getString(R.string.title_git_api);
                    ;
                    url = getResources().getString(R.string.url_git_api);
                    break;
                }

                case R.id.nav_github_repository: {
                    title = getResources().getString(R.string.title_bitbucket_repository);
                    ;
                    url = getResources().getString(R.string.url_git_repository);
                    break;
                }

                default: {
                }

            }

            GDialogWebview gDialogWebview = new GDialogWebview();
            gDialogWebview.context = this;
            gDialogWebview.title = title;
            gDialogWebview.url = url;
            gDialogWebview.OnView();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

            return true;
        } catch (Exception e) {
            IuLog.Error(e);
        }
        return false;
    }


    public void OnClickListStargazers(View view) {
        try {

            EGitHubRepository eGitHubRepository = new EGitHubRepository();
            eGitHubRepository.iD = IuKey.GenerateID();

            eGitHubRepository.user = textInputGitHubUser.getText().toString();
            eGitHubRepository.repository = textInputGitHubRepository.getText().toString();

            gDialogProgress = new GDialogProgress();
            gDialogProgress.context = this;
            gDialogProgress.title = "";
            gDialogProgress.message = getResources().getString(R.string.message_gdialogprogress);
            gDialogProgress.OnView();


            CGithubFlow cGithubFlow = new CGithubFlow();
            cGithubFlow.delegate = this;
            cGithubFlow.OnListStargazers(eGitHubRepository);

        } catch (Exception e) {
            IuLog.Error(e);
        }

    }

    @Override
    public void OnSuccess(EObject eObject) {
        try {
            gDialogProgress.OnHide();
            EGitHubRepository eGitHubRepository = (EGitHubRepository) eObject;
            IuFoundation.DoSet(eGitHubRepository);

            if (eGitHubRepository.arrayListStargazer.size() == 0) {
                GDialogAlert gDialogAlert = new GDialogAlert();
                gDialogAlert.context = this;
                gDialogAlert.title = "";
                gDialogAlert.message = getResources().getString(R.string.message_no_stargazers);
                gDialogAlert.OnView();
            } else {
                Intent intent = new Intent(this, ActivityListStargazers.class);
                intent.putExtra("iD", eGitHubRepository.iD);
                startActivity(intent);
            }

        } catch (Exception e) {
            IuLog.Error(e);
        }
    }

    @Override
    public void OnError(Exception exception) {
        try {
            gDialogProgress.OnHide();
            GDialogAlert gDialogAlert = new GDialogAlert();
            gDialogAlert.context = this;
            gDialogAlert.title = "Error Repository";
            gDialogAlert.message = exception.getMessage();
            gDialogAlert.OnView();
        } catch (Exception e) {
            IuLog.Error(e);
        }
    }
}
