package evo.framework.utility.log;

import evo.framework.entity.ELog;

public class ULog {

    static ULog instance;

    public static  String tag="TAG";

    private ULog() {
    }

    public static ULog Instance() {
        if (instance == null) {
            instance = new ULog();
        }
        return instance;
    }

    public void Log(ELog eLog)

    {
        try {
          //  String tag = CBoot.GetActivityContext().getPackageName().substring(0, 22);



            if (eLog.tag == null) {
                eLog.tag = "";
            }

            if (eLog.message == null) {
                eLog.message = "";
            }


            if (eLog.parameter == null) {
                eLog.parameter = "";
            }

            if (eLog.tag.equals(ELog.Debug)) {

                android.util.Log.d(tag + "-" + eLog.message, eLog.parameter.toString());
            }

            if (eLog.tag.equals(ELog.Warning)) {
                android.util.Log.w(tag + "-" + eLog.message, eLog.parameter.toString());
            }

            if (eLog.tag.equals(ELog.Error)) {
                android.util.Log.e(tag + "-" + eLog.message, eLog.parameter.toString());
            }

            if (eLog.tag.equals(ELog.Error_EXCEPTION)) {
                android.util.Log.e(tag + "-" + eLog.message, eLog.parameter.toString() + "\n" + eLog.trace);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
