package evo.framework.utility.message;

import android.util.Log;

import java.util.ArrayList;
import java.util.TreeMap;

import evo.framework.utility.UObject;
import evo.framework.entity.EMessage;

public class UMessage extends UObject {

	static UMessage instance;
	public TreeMap<String, ArrayList<EvoCallBack>> mapObserver = new  TreeMap<String, ArrayList<EvoCallBack>> ();
	
	private UMessage ()	{
		try {
	
		} catch (Exception e) {
			DoError (e);
		}
		
	}

	public static UMessage Instance ()
	{
		if (instance == null) {
			instance = new UMessage ();
		}
		return instance;
	}

	public void AddObserver (String eventType, EvoCallBack handler)
	{
		try {
			if (!mapObserver.containsKey (eventType)) {
				ArrayList arrayObserver = new ArrayList ();
					arrayObserver.add (handler);
				mapObserver.put (eventType, arrayObserver);	
			} else {
				ArrayList arrayObserver = mapObserver.get(eventType);
				arrayObserver.add (handler);
			}
			
		} catch (Exception e) {
			DoError (e);
		}
		
		
	}

	public void RemoveObserver (String eventType, EvoCallBack handler)
	{
		try {
			if (mapObserver.containsKey (eventType)) {
				ArrayList arrayObserver = mapObserver.get(eventType);
					arrayObserver.remove (handler);
			
					if (arrayObserver.size() == 0) {
						mapObserver.remove(eventType);
					}
			}
		} catch (Exception e) {
			DoError (e);
		}
		
	}

	public void OnNotify (EMessage eMessage)
	{
		try {

			Log.w("OnNotify", mapObserver.keySet().toString());
			if (mapObserver.containsKey (eMessage.message)) {
				ArrayList<EvoCallBack> arrayObserver = mapObserver.get (eMessage.message);	
			
				try {
					
					for (EvoCallBack evoCallBack : arrayObserver) {
						try {
							evoCallBack.OnNotify(eMessage);
						} catch (Exception e) {
							DoWarning (e);
						}					
					}
				} catch (Exception e) {
					DoWarning (e);
				}
			}
		} catch (Exception e) {
			DoError (e);
		}
	}
}
