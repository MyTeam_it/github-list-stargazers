package evo.framework.utility.message;

import java.lang.reflect.*;

import android.util.Log;
import evo.framework.EvoObject;
import evo.framework.utility.UObject;

public class EvoCallBack extends UObject {
	private String methodName;
	private EvoObject evoObject;

	public EvoCallBack(EvoObject evoObject, String methodName) {
		try {
			this.methodName = methodName;
			this.evoObject = evoObject;
		} catch (Exception e) {

		}
	}

	public Object OnNotify(Object... parameters) {
		try {
			Log.d("OnNotify", evoObject.getClass().getName() + " => " + methodName);
			Method method = evoObject.getClass().getMethod(methodName,
					getParameterClasses(parameters));
			return method.invoke(evoObject, parameters);
		} catch (Exception e) {
			DoError(e);
		}
		return null;
	}

	private Class[] getParameterClasses(Object... parameters) {
		Class[] classes = new Class[parameters.length];
		for (int i = 0; i < classes.length; i++) {
			classes[i] = parameters[i].getClass();
		}
		return classes;
	}
}