package evo.framework.utility.message;

import evo.framework.entity.EMessage;


public class IuMessage {

    public static Boolean isTest = false;

    public static void AddListener(String message, EvoCallBack handler) {
        UMessage.Instance().AddObserver( message, handler );
    }

    public static void RemoveListener(String message, EvoCallBack handler) {
            UMessage.Instance().RemoveObserver( message, handler );
    }

    public static void Notify(EMessage eMessage) {
        UMessage.Instance().OnNotify( eMessage );
    }
}
