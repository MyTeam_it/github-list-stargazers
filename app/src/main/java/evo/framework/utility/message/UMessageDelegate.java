package evo.framework.utility.message;

import evo.framework.entity.EMessage;


public interface UMessageDelegate {

    public void OnNotifyBack(EMessage eMessage);

}
