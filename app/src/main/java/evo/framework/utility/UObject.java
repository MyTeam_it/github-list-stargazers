package evo.framework.utility;

import evo.framework.entity.ELog;
import evo.framework.entity.EMessage;
import evo.framework.utility.key.IuKey;
import evo.framework.utility.log.IuLog;
import evo.framework.utility.message.IuMessage;

public class UObject {
	
	protected void DoLog (String tag,String message, Object parameter)
	{
		try {
			ELog eLog = ToELog(tag, message,parameter);
			IuLog.Log(eLog);
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoDebug (Exception exception)
	{
		try {
			DoLog (ELog.Debug, exception.getMessage(), exception.getStackTrace());
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoDebug (String message)
	{
		try {
			DoLog (ELog.Debug, message, null);
		} catch (Exception e) {
			DoError(e);
		}
	}
	
	protected void DoDebug (String message, String parameter)
	{
		try {
			DoLog (ELog.Debug, message, null);
		} catch (Exception e) {
			DoError(e);
		}
	}
	
	
	protected  void DoWarning (Exception exception)
	{
		try {
			DoLog (ELog.Warning, exception.getMessage(), exception.getStackTrace());
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected  void DoWarning (String message)
	{
		try {
			DoLog (ELog.Warning, message, null);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected  void DoWarning (String message, Object parameter)
	{
		try {
			DoLog (ELog.Warning, message, parameter);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}
	

	protected  void DoError (Exception exception)
	{
		try {
			String stackTraceMessage = "";
			for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
				stackTraceMessage +=   stackTraceElement.getClassName() + " " + stackTraceElement.getMethodName() + " " + stackTraceElement.getLineNumber() + "\n";
			}
			
			
			DoLog (ELog.Error, "ERROR:" + exception.getLocalizedMessage(),   getClass().getName() + "\n" +exception.getCause() + "\n" + exception.getMessage() + "\n" + stackTraceMessage);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected  void DoError (String message)
	{
		try {
			DoLog (ELog.Error, message, null);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}


	protected void DoNotify (String message, Object parameter)
	{
		try {
			DoNotify (ToEMessage (message, parameter));
			
		} catch (Exception e) {
			DoError (e);
		}
	}
	
	protected void DoNotify (EMessage eMessage)
	{
		try {
			IuMessage.Notify (eMessage);

		} catch (Exception e) {
			DoError (e);
		}
	}
	


	protected EMessage ToEMessage (String message)
	{
		try {
			return ToEMessage (message, null, null);
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}
	
	protected EMessage ToEMessage (String message, Object parameter)
	{
		try {
			return ToEMessage (message, parameter, null);
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}
	
	protected EMessage ToEMessage (String message, Object parameter, String messageBack)
	{
		try {
			EMessage eMessage = new EMessage ();
			eMessage.iD = IuKey.GenerateID();
			eMessage.time = IuKey.GenerateTime ();
			eMessage.message = message;
			eMessage.parameter = parameter;
			eMessage.messageBack = messageBack;	
			return eMessage;
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}
	
	

	protected ELog ToELog (String tag,Exception exception)
	{
		try {
			return ToELog (tag ,exception.getMessage() ,exception.getStackTrace());
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}
	

	
	protected ELog ToELog (String tag,String message, Object parameter)
	{
		try {
			ELog eLog = new ELog ();
			eLog.iD = IuKey.GenerateID ();
			eLog.tag = ELog.Debug;
			eLog.time = IuKey.GenerateTime ();

			eLog.trace = this.getClass().getName().toString() + ":"  + String.valueOf(this.hashCode()).replace("-","0") ;
			eLog.message = message;
			eLog.parameter = parameter;
			return eLog;
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}
	
	protected void DoErrorNotify (String message, ELog eLog)
	{
		try {
			DoError (message);
			DoNotify (message, eLog);		
		} catch (Exception e) {
			DoError (e);
		}
	}
	
	protected void DoErrorNotify (String message, Exception exception)
	{
		try {
			DoError (exception);
			DoNotify (message, exception.getMessage());		
		} catch (Exception e) {
			DoError (e);
		}
	}
	
	/*
	protected EQuery ToEQuery(EObject eObject, bool isMediator,bool isFoundation, bool isCrypt){
		try {
			EQuery eQuery = new EQuery();
			eQuery.iD = IuKey.GenerateId();
			eQuery.time = IuKey.GenerateTime();
			eQuery.eObject = eObject;
			eQuery.isMediator = isMediator;
			eQuery.isFoundation = isFoundation;
			eQuery.isCrypt = isCrypt;
			return eQuery;		
		} catch (Exception e) {
			DoError (e);
		}
		return null;
	}*/
	
}
