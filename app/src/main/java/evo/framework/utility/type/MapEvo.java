package evo.framework.utility.type;

import java.util.TreeMap;

import evo.framework.entity.EObject;
import evo.framework.utility.key.IuKey;

public class MapEvo extends TreeMap<String,Object> {
  /*  public TreeMap<String,Object> Clone ()
    {
        try {

            TreeMap<String,Object> mapReturn = new TreeMap<String,Object>();
            for(String key : this.keySet()){
                mapReturn.put(key, get(key));

            }
            return mapReturn;
        } catch (Exception e) {
         //   UObject(e);
        }
        return null;
    }
*/

    public Boolean Contain (EObject eObject)
    {
        try {
            if (eObject != null) {
                if (eObject.iD != null) {
                    return this.containsKey(eObject.iD);
                }
            }
        } catch (Exception e) {
           // UObject.DoError(e);
        }
        return false;
    }

    public Object DoGet (String key)
    {
        try {
           //  //Log.w("DoGet", this.toString());
            if (this.containsKey(key)) {
                //Log.e("2MAPEVODoGet", key);

             Object obj=   this.get(key);
                if(obj == null){
                    //Log.w("--->OBJ ","NULLLLLLLLLLL!!!");

                }else {
                    //Log.w("--->Parameter ", obj.toString());
                }

                return this.get(key);
            }else {
                //Log.w("NOT CONTAin", key);
            }
        } catch (Exception e) {

        }
        return null;
    }

    public void DoSet (String key, Object value)
    {
        try {
            if (key != null) {

                    this.put (key, value);

            }
        } catch (Exception e) {
           // UObject.DoError (e);
        }

    }

    public void DoSet (EObject eObject)
    {
        try {
            if (eObject.iD == null) {
                eObject.iD = IuKey.GenerateID ();
            }

            if (eObject.iD.equals("")) {
                eObject.iD = IuKey.GenerateID();
            }


                this.put(eObject.iD, eObject);

        } catch (Exception e) {
           // UObject.DoError (e);
        }

    }

    public void DoDel (EObject eObject)
    {
        try {
            if (eObject.iD != null) {
                eObject.iD = IuKey.GenerateID();


                if (containsKey(eObject.iD)) {
                    this.remove(eObject.iD);
                }
            }
        } catch (Exception e) {
           // UObject.DoError (e);
        }

    }

     public String toString ()
    {
        try {
            String strReturn = "Map " + "[" + this.size() + "]\n";
            int i = 0;

            for (String key : this.keySet()) {
                try {
                    strReturn += "\tkey: [" + String.valueOf (i) + "]\n" + key + "\n\tvalue:\n" + DoGet(key).toString() + "\n";
                    i += 1;

                } catch (Exception e) {
                   // UObject.DoError (e);
                }
            }

            return strReturn;
        } catch (Exception e) {
           // UObject.DoError (e);
        }
        return null;
    }




}
