package evo.framework.utility.type;


import evo.framework.entity.EObject;

public class IuType {

    public static Boolean isEObject( Object obj){
        return obj.getClass().getSuperclass().equals(EObject.class);
    }



    public static Boolean isMap( Object obj){
        return obj .getClass().equals(MapEvo.class);
    }

    public static Boolean isArrayByte( Object obj){

        return obj .getClass().equals(Byte[].class);
    }


}
