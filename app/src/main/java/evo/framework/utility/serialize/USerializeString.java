package evo.framework.utility.serialize;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import evo.framework.entity.EObject;
import evo.framework.utility.UObject;
import evo.framework.utility.type.MapEvo;

public class USerializeString extends UObject {

    private static volatile USerializeString instance;
    protected Gson gSon;

    public MapEvo mapEObject = new MapEvo();

    public static USerializeString getInstance() {
        if (instance == null) {
            instance = new USerializeString();
        }
        return instance;
    }

    private USerializeString() {

    }


    public String ToJson(EObject eObject) {
        try {

            if(gSon == null){
                gSon = new Gson();
            }


            return gSon.toJson(eObject);
        } catch (Exception e) {
            DoError(e);
        }

        return null;
    }

    public EObject FromJson(String str,Type t) {
        try {

            if(gSon == null){
                gSon = new Gson();
            }


            return gSon.fromJson(str,t);

        } catch (Exception e) {
            DoError(e);
        }
        return null;
    }


}
