package evo.framework.utility.serialize;

import java.lang.reflect.Type;

import evo.framework.entity.EObject;

public class IuSerialize {

	public static String ToJson (EObject eObject)
	{
		return USerializeString.getInstance().ToJson(eObject);
	}

	public static EObject FromJson(String str, Type type)
	{
		return USerializeString.getInstance().FromJson (str,type);
	}
	
}
