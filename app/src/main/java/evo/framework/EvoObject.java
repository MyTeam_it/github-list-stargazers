package evo.framework;

import android.content.Context;

import evo.framework.entity.ELog;
import evo.framework.entity.EMessage;
import evo.framework.utility.key.IuKey;
import evo.framework.utility.log.IuLog;
import evo.framework.utility.message.EvoCallBack;
import evo.framework.utility.message.IuMessage;

public class EvoObject {

	public Context context;

	public String name;


	public void OnStart(EMessage eMessage) {
		try {

		} catch (Exception e) {

		}
	}

	public void OnDidLoad(EMessage eMessage) {
		try {

		} catch (Exception e) {

		}
	}

	protected void AddListener(String message, EvoCallBack evoCallBack) {
		try {
			IuMessage.AddListener(message, evoCallBack);
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void RemoveListener(String message, EvoCallBack evoCallBack) {
		try {
			IuMessage.RemoveListener(message, evoCallBack);
		} catch (Exception e) {
			DoError(e);
		}
	}



	protected void DoDebug(Exception exception) {
		try {
			DoLog(ELog.Debug, exception.getMessage(), exception.getStackTrace());
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoDebug(EMessage eMessage) {
		try {
			DoLog(ELog.Debug, eMessage.message, eMessage.parameterJson);
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoDebug(String message) {
		try {
			DoLog(ELog.Debug, message, null);
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoDebug(String message, String parameter) {
		try {
			DoLog(ELog.Debug, message, null);
		} catch (Exception e) {
			DoError(e);
		}
	}

	protected void DoWarning(Exception exception) {
		try {
			DoLog(ELog.Warning, exception.getMessage(),
					exception.getStackTrace());
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected void DoWarning(String message) {
		try {
			DoLog(ELog.Warning, message, null);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected void DoWarning(String message, Object parameter) {
		try {
			DoLog(ELog.Warning, message, parameter);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected void DoError(Exception exception) {
		try {
			String stackTraceMessage = "";
			for (StackTraceElement stackTraceElement : exception
					.getStackTrace()) {
				stackTraceMessage += stackTraceElement.getClassName() + " "
						+ stackTraceElement.getMethodName() + " "
						+ stackTraceElement.getLineNumber() + "\n";
			}

			DoLog(ELog.Error, "ERROR",
					getClass().getName() + "\n" + exception.getMessage() + "\n"
							+ "\n" + stackTraceMessage);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}

	protected void DoError(String message) {
		try {
			DoLog(ELog.Error, message, null);
		} catch (Exception e) {
			IuLog.Error(e);
		}
	}




	protected ELog ToELog(String tag, Exception exception) {
		try {
			return ToELog(tag, exception.getMessage(),
					exception.getStackTrace());
		} catch (Exception e) {
			DoError(e);
		}
		return null;
	}

	protected ELog ToELog(String tag, String message, Object parameter) {
		try {
			ELog eLog = new ELog();
			eLog.iD = IuKey.GenerateID();
			eLog.tag = ELog.Error_EXCEPTION;
			eLog.time = IuKey.GenerateTime();
			eLog.trace = this.getClass().getName().toString() + ":"
					+ String.valueOf(this.hashCode()).replace("-", "0");
			eLog.message = message;
			eLog.parameter = parameter;
			return eLog;
		} catch (Exception e) {
			DoError(e);
		}
		return null;
	}



	protected void DoLog(String tag, String message, Object parameter) {
		try {
			ELog eLog = ToELog(tag, message, parameter);
			IuLog.Log(eLog);
		} catch (Exception e) {
			DoError(e);
		}
	}
}
