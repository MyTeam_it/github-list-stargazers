package evo.framework.entity;


public class ELog extends EObject {

	public static  String Debug = "Debug";
	public static  String Error = "Error";
	public static  String Error_EXCEPTION = "Exception";
	public static  String Warning = "Warning";
	public static  String Notify = "Notify";
	public static  String NotifyToDevice = "NotifyToDevice";
	public static  String NotifyFromDevice = "NotifyFromDevice";
	public static  String NotifyNetOut = "NotifyNetOut";
	public static  String NotifyNetIn = "NotifyNetIn";
	
	public String trace;

	public String tag ;

	public long tick ;

	public long tickOffset;

	public String message ;

	public Object parameter;
}
