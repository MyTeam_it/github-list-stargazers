package evo.framework;

import evo.framework.entity.EObject;

public interface IEvoCallback {

    public void OnSuccess(EObject eObject);

    public void OnError(Exception e);

}
