package evo.packages.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import evo.framework.graphic.GObject;

public class GDialogWebview extends GObject {


    public String title;
    public String url;
    public String titleClose = "Close";

    public void OnView() {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);

            WebView webView = new WebView(context);
            webView.loadUrl(url);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });

            alert.setView(webView);
            alert.setNegativeButton(titleClose, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            DoError(e);
        }
    }

    public void OnHide() {

    }
}
