package evo.packages.dialog;

import android.app.ProgressDialog;

import evo.framework.graphic.GObject;

public class GDialogProgress extends GObject {

    public String title;
    public String message;


    protected ProgressDialog progressDialog;

    public void OnView() {
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(true);
            progressDialog.show();
        } catch (Exception e) {
            DoError(e);
        }
    }

    public void OnHide() {
        try {
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            DoError(e);
        }

    }
}
