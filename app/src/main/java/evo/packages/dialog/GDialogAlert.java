package evo.packages.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;

import evo.framework.graphic.GObject;

public class GDialogAlert extends GObject {

    public String title;
    public String message;
    public String url;
    public String titleClose = "Close";

    public void OnView() {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);
            alert.setMessage(message);

            alert.setNegativeButton(titleClose, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            DoError(e);
        }
    }

    public void OnHide() {

    }
}