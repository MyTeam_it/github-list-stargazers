package evo.packages.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.widget.VideoView;

import evo.framework.graphic.GObject;

public class GDialogVideoview extends GObject{

    public String title;
    public String url;
    public String titleClose = "Close";

    public void OnView() {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);
            Uri uri = Uri.parse(url);
            VideoView videoView = new VideoView(context);
            videoView.setVideoURI(uri);
            videoView.start();

            alert.setView(videoView);
            alert.setNegativeButton(titleClose, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            alert.show();

        } catch (Exception e) {
            DoError(e);
        }
    }

    public void OnHide() {

    }
}
