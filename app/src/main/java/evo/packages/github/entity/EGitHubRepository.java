package evo.packages.github.entity;

import java.util.ArrayList;

import evo.framework.entity.EObject;
import evo.framework.utility.type.MapEvo;

public class EGitHubRepository extends EObject {

    public String user;
    public String repository;
    public MapEvo mapUserStargazers;
    public ArrayList<EGithubUser> arrayListStargazer;

}
