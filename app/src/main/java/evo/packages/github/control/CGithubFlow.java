package evo.packages.github.control;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import evo.framework.IEvoCallback;
import evo.framework.control.CObject;
import evo.framework.entity.EObject;
import evo.framework.utility.serialize.IuSerialize;
import evo.packages.github.entity.EGitHubRepository;
import evo.packages.github.entity.EGithubUser;

public class CGithubFlow extends CObject {

    public final static String URL_GitHub_v3_Stargazers = "https://api.github.com/repos/%s/%s/stargazers";
    public final static String USER_AGENT = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30";

    public IEvoCallback delegate;

    public void OnListStargazers(EObject eObject) {
        try {

            final EGitHubRepository eGitHubRepository = (EGitHubRepository) eObject;

            String url = String.format(URL_GitHub_v3_Stargazers, eGitHubRepository.user, eGitHubRepository.repository);
            DoDebug("url:" + url);

            AsyncHttpClient client = new AsyncHttpClient();
            client.setUserAgent(USER_AGENT);
            client.setTimeout(20);
            client.get(url, new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray arrayJson) {
                            try {
                                eGitHubRepository.arrayListStargazer = new ArrayList<EGithubUser>();

                                for (int i = 0; i < arrayJson.length(); i++) {
                                    try {
                                        String jsonObjectStr = arrayJson.getString(i);
                                        EGithubUser eGithubUser = (EGithubUser) IuSerialize.FromJson(jsonObjectStr, EGithubUser.class);
                                        eGitHubRepository.arrayListStargazer.add(eGithubUser);
                                    } catch (Exception e) {
                                        DoError(e);
                                    }
                                }
                                delegate.OnSuccess(eGitHubRepository);
                            } catch (Exception e) {
                                DoError(e);
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject jsonObject) {
                            delegate.OnError(new Exception(throwable.getMessage(), throwable));
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            delegate.OnError(new Exception(throwable.getMessage(), throwable));
                        }

                    }
            );


        } catch (Exception e) {
            DoError(e);
        }
    }

}
