package evo.packages.foundation;

import evo.framework.entity.EObject;

public class IuFoundation {

    public static void DoSet(EObject eObject){
        UFoundation.Instance().DoSet(eObject);
    }

    public static EObject DoGetOne(String iD){
        return UFoundation.Instance().DoGetOne(iD);
    }

    public static void DoDel(EObject eObject){
        UFoundation.Instance().DoDel(eObject);
    }
}
