package evo.packages.foundation;


import evo.framework.entity.EObject;
import evo.framework.utility.UObject;
import evo.framework.utility.type.MapEvo;

class UFoundation extends UObject {

    protected MapEvo mapEvo = new MapEvo();

    private static UFoundation instance;


    private UFoundation() {
    }

    public static UFoundation Instance() {
        if (instance == null) {
            instance = new UFoundation();
        }
        return instance;
    }

    public void DoSet(EObject eObject) {
        try {
            mapEvo.DoSet(eObject);

        } catch (Exception e) {
            DoError(e);
        }
    }

    public EObject DoGetOne(String iD) {
        try {
            return (EObject) mapEvo.DoGet(iD);
        } catch (Exception e) {
            DoError(e);
        }
        return null;
    }

    public void DoDel(EObject eObject) {
        try {
            mapEvo.DoDel(eObject);
        } catch (Exception e) {
            DoError(e);
        }

    }
}
