package evo.packages.validator;

import android.text.Editable;

import evo.framework.utility.log.IuLog;

public class IuValidator {

	public static boolean IsEmpty(Object object) {
		try {
			if (object == null) {
				return true;
			}
		} catch (Exception e) {
			IuLog.Error(e);
		}
		return false;
	}


	public static boolean IsValid(Object object) {
		try {
			return  !IsEmpty(object);
		} catch (Exception e) {
			IuLog.Error(e);
		}
		return false;
	}

	public static boolean IsValid(Editable editable) {
		try {

			if(editable!=null){
				if(!editable.toString().equals("")){
					return true;
				}
			}

		} catch (Exception e) {
			IuLog.Error(e);
		}
		return false;
	}

}
