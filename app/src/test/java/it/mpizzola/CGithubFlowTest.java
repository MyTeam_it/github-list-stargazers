package it.mpizzola;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import evo.framework.IEvoCallback;
import evo.framework.entity.EObject;
import evo.framework.utility.log.IuLog;
import evo.packages.github.control.CGithubFlow;
import evo.packages.github.entity.EGitHubRepository;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class CGithubFlowTest implements IEvoCallback {

    @Before
    public void setUp() throws Exception {
        IuLog.isProduction = true;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onListStargazers() {

        EGitHubRepository eGitHubRepository = new EGitHubRepository();
        eGitHubRepository.user = "google";
        eGitHubRepository.repository = "gson";

        CGithubFlow cGithubFlow = new CGithubFlow();
        cGithubFlow.delegate = this;
        cGithubFlow.OnListStargazers(eGitHubRepository);
    }

    @Override
    public void OnSuccess(EObject eObject) {
        assertNotNull("NotNull", eObject);
        EGitHubRepository eGitHubRepository = (EGitHubRepository) eObject;
        assertNotEquals("Size 0", eGitHubRepository.arrayListStargazer.size());
    }

    @Override
    public void OnError(Exception e) {

        fail("OnError:" + e.toString());
    }
}