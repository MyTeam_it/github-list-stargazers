# Github List Stargazers Android App
![logo](https://assets-cdn.github.com/images/modules/logos_page/Octocat.png)

 Android app project that through the API GitHub allows the display of the list of stargazers of a repository, that is the list of users who have inserted among the favorites.
 
The app must allow the user to type owner and repository name and finally show the list (including Avatars and username) of the users who have entered the bookmark.

## Doc
https://githubstargazers.herokuapp.com

## Build tool
* Android Studio 3.1.4
https://developer.android.com/studio/
* Help 
https://developer.android.com/studio/run/

## Android Project
* minSdkVersion 23

## Framework
* Evo Framework (https://www.slideshare.net/MassimilianoPizzola/evo-framework-my-app)

## Github api
https://developer.github.com/v3/activity/starring/#list-stargazers

## Maven external dependencies
* com.google.code.gson:gson:2.2.4
* com.loopj.android:android-async-http:1.4.9
* com.squareup.picasso:picasso:2.71828

## Drawable Vector 
* https://github.com/logos
* https://www.flaticon.com